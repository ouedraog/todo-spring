package bf.isge.ic3.todoapi.service;

import bf.isge.ic3.todoapi.model.User;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService{
    private final Map<Integer, User> userStore = new HashMap<>();
    @Override
    public Optional<User> findById(int id) {
        return Optional.ofNullable(userStore.get(id));
    }

    @Override
    public User addUser(User user) {
        user.setId(userStore.size());
        userStore.put(user.getId(), user);
        return user;
    }
}
