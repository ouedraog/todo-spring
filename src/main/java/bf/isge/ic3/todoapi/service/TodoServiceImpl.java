package bf.isge.ic3.todoapi.service;

import bf.isge.ic3.todoapi.dto.CreateTodoDto;
import bf.isge.ic3.todoapi.dto.UpdateTodoDto;
import bf.isge.ic3.todoapi.exception.TodoNotFoundException;
import bf.isge.ic3.todoapi.model.Todo;
import bf.isge.ic3.todoapi.model.User;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
@Service
public class TodoServiceImpl implements TodoService{
    private final Map<Integer, Todo> todoStore = new HashMap<>();
    private final UserService userService;

    public TodoServiceImpl(UserService userService) {
        this.userService = userService;
    }

    @Override
    public Optional<Todo> findById(int id) {
        return Optional.ofNullable(todoStore.get(id));
    }

    @Override
    public Todo createTodo(CreateTodoDto todoDto) {
        Todo todo = new Todo();
        todo.setId(todoStore.size());
        todo.setTitle(todoDto.getTitle());
        todo.setDescription(todoDto.getDescription());
        todo.setArchived(false);
        todo.setCreatedAt(LocalDateTime.now());
        Optional<User> author = userService.findById(todoDto.getAuthorId());
        todo.setAuthor(author.orElse(null));
        todoStore.put(todo.getId(), todo);
        return todo;
    }

    @Override
    public Todo updateTodo(int id, UpdateTodoDto todoDto) throws TodoNotFoundException {
        Optional<Todo> todoOptional = findById(id);
        if(todoOptional.isEmpty()) {
            throw new TodoNotFoundException();
        }
        Todo todo = todoOptional.get();
        if(todoDto.getTitle() != null) {
            todo.setTitle(todoDto.getTitle());
        }
        if(todoDto.getDescription() != null) {
            todo.setDescription(todoDto.getDescription());
        }
        return todo;
    }

    @Override
    public boolean removeTodo(int id) {
        return todoStore.remove(id) != null;
    }

    @Override
    public List<Todo> findAll() {
        return new ArrayList<>(todoStore.values());
    }
}
