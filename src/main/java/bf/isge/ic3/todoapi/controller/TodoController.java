package bf.isge.ic3.todoapi.controller;

import bf.isge.ic3.todoapi.dto.CreateTodoDto;
import bf.isge.ic3.todoapi.dto.UpdateTodoDto;
import bf.isge.ic3.todoapi.exception.TodoNotFoundException;
import bf.isge.ic3.todoapi.model.Todo;
import bf.isge.ic3.todoapi.service.TodoService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/todos")
public class TodoController {
    private final TodoService todoService;

    public TodoController(TodoService todoService) {
        this.todoService = todoService;
    }
    @PostMapping("")
    public Todo createTodo(@RequestBody CreateTodoDto createTodoDto) {
        return todoService.createTodo(createTodoDto);
    }
    @GetMapping("")
    public List<Todo> findAllTodos() {
        return todoService.findAll();
    }
    @GetMapping("{id}")
    public ResponseEntity<?> findTodoById(@PathVariable("id") int id) throws TodoNotFoundException {
        Optional<Todo> todo = todoService.findById(id);
        if(todo.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(todo.get());
    }
    @DeleteMapping("{id}")
    public void removeTodo(@PathVariable("id") int id) {
        todoService.removeTodo(id);
    }
    @PutMapping("{id}")
    public Todo updateTodo(@PathVariable("id") int id, @RequestBody UpdateTodoDto updateTodoDto) throws TodoNotFoundException {
        return todoService.updateTodo(id, updateTodoDto);
    }
}
