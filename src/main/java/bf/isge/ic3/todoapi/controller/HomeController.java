package bf.isge.ic3.todoapi.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {
    @GetMapping("/")
    public String sayHelloWorld() {
        return "Hello world";
    }

    @GetMapping("/hello/{name}") // GET /hello/paul
    public String hello(@PathVariable("name") String name) {
        return "Good morning "+ name;
    }

    @GetMapping("/hello") //  GET /hello?name=paul&age=10
    public String hellWithParams(@RequestParam("name") String name,
                                 @RequestParam("age") int age) {
        return "Good morning "+ name + ", your age is " + age;
    }
}
