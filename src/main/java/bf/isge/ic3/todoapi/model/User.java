package bf.isge.ic3.todoapi.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class User {
    private int id;
    private String name;
    @JsonIgnore
    private List<Todo> todoList = new ArrayList<>();
}
