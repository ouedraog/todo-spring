package bf.isge.ic3.todoapi.model;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Todo {
    private int id;
    private String title;
    private String description;
    private User author;
    private LocalDateTime createdAt;
    private boolean archived;

}
