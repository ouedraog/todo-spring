package bf.isge.ic3.todoapi.dto;

import lombok.Data;

@Data
public class CreateTodoDto {
    private String title;
    private String description;
    private int authorId;
}
